# Event Driven Web Analytics Tracking

## Introduction

Most web analytics these days are still focussed on traditional page based web sites. Most analytics tracking code is loaded and triggered based on a page load. However in new and emerging web technology, sites are build with frameworks that are application driven, and present a whole website within one single web page (Single Page Application). Besides, sites use more and more widget style of controls, that will manipulate the DOM, and are in a highly sense client-side driven. Furthermore, with a lot of mobile clients coming in the picture, the actual content is often quite limited in the view of the visitor. The traditional triggers for analytics need to move towards the coded area of a website.

Web analytics implementors need a new way to align business needs for online data gathering with the new style of website coding.

The approach should

* allow IT and front-end developers to integrate analytics into their way of working, adapting test-driven development with unit-tests and end-to-end integration tests on the analytics implementation as well
* make the embedded analytics tracking code methodology as much as possible independent from the CMS, technology and the Analytics platforms chosen
* allow the web analysts to have enough flexibility on the business rules needed for proper and complete tracking and avoid misuse of the tag management system for scripting business logic; business logic should be doable with the basic set of selection rules offered by the tag management system

## Solution

#### Events

In the event based tracking methodology we consider all web page actions to be events.

![page events](images/events.png)

Showing the page will give us a page impression, showing an object on the page will give an impression of that object; clicking on an object or doing any other user related action will emit the corresponding event towards our tracking mechanism. In this sense, we can also capture specific conditions like being on the actual screen, or appearing when scrolling. The front-end developers mostly have control over objects and interactions. Therefor they are considered to be the initiators of the event based tracking.

In the methodology, we will let front-end developers generate events on things happening on the site, whether it is an object being shown or any other direct user interaction. The event is emitted to a framework that will implement the tag management and analytics, capture the event and translate it based on rules set by web analysts.

![developer events](images/developer.png)

#### Tag Management System agnostic

The basics of the methodology is separating the implementation of web-analytics from the tools used for measuring. Good web-analytics are based on consistent and qualitative implementation of a data layer on the web site. However, this datalayer must be flexible enough to support new and / or different implementation technologies, and it should not lock-in a specific
vendor based analytics or tag management system. On the other side, vendor specific implementations are still the most accurate, easy to configure and best performing solutions.

![TMS agnostic](images/tmsagnostic.png)

The framework will use a separation layer, based on a messaging system to overcome this problem. Events are emitted in a standardized format and mechanism (JavaScript mediator or observer pattern with JSON object event structure), and in second stage translated by a custom generic component to a vendor specific datalayer implementation.

![Distributed Model](images/distrimodel.png)

#### Data Attributes

We can take analytics data abstraction one step beyond the programming level. We can attribute analytics data on the content level. This will make the operational team (content managers / web analysts) responsible for the contents of the analytics. The development team can once develop generic principles for reading the attributes and passing them through the event chain at the right moment, on the right interaction. This makes the implementation semi data driven, and relieve the development team. We define objects or components to have events and tracking information, embedded in the HTML code. This information can be added at the templating level in the back-end of the CMS. So no or minimal need for manual customizations by the content manager.

![Data attribution](images/attribution.png)

#### Testing

Using this approach allows us to ensure online data quality in several ways. With the attribution layer addition, we can parse web pages for analytics data attributes defined and verify them with a reference set. This can be done in a visual attractive way, giving content managers, developers and analysts direct insight into the content and quality of the analytics attributes.

![QA data attribution](images/qatags.png)

The event chain mechanism allows us to listen, with custom code, to events passing the chain, and also verifying if they should appear, if they are well formatted and contain the correct information. These mechanisms are easy to implement in automated test suites for both unit and end-to-end testing. This allows us to lock-in quality control of online data capturing, and makes it part of the normal development process. The information contained in the events must not be limited to the attributed values. If back-end data is passing the front-end and needs to be added to the Digital Data, it can easily be added to the event data structure from front-end javascript code. This is also the case for localStorage and or Cookie data that should be passed.

![QA events](images/qaevents.png)



## Demo Applications
* Demo website
* Demo website in React
* Demo iFrame tracking: iFrame subsite
* Demo webserver
    * starts main website, iFrame site, visitor ID tracking site in virtual domains on port 3000
    * with simplified Visitor ID tracking

Note: demo sites requires these domains to be mapped to localhost (/etc/hosts):
* www.mytest.local
* www.mytest2.local
* iframe.mytest2.local
* vid.mytest.local
* vid.mytest2.local

##### Client-side analyticstracker module

The demo applications use the [analyticstracker](https://bitbucket.org/xploregroup/xploregroup-webanalytics-analyticstracker) module for implementing the Event Driven Web Analytics Tracking.

## Changelog

##### version 0.1.1
* Webserver start
* iFrame tracking
* Simplified Visitor ID tracking

##### version 0.1.0
* Intitial version

## Authors

* **Stefan Maris** - *Initial work* - [Xplore Group](http://www.xploregroup.be)

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details

## Acknowledgments

* Based on POC implementation at Essent.nl
