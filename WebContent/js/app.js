require.config({
    baseUrl: '/js/lib',
    paths: {
        app: '../app',
        shop: '../app/shop',
        'mediator-js': 'mediator-js/mediator.min',
        stitch: 'stitch-js-sdk/dist/web/stitch.min',
        analyticstracker: 'analyticstracker/analyticstracker',
        analyticsTransQA: 'analytics-transQA/analyticsTransQA',
        analyticsTransDTM: 'analytics-transDTM/analyticsTransDTM',
        analyticsTransGTM: 'analytics-transGTM/analyticsTransGTM',
        analyticsTransGTMEE: 'analytics-transGTMEE/analyticsTransGTMEE',
        analyticsTransSFDMP: 'analytics-transSFDMP/analyticsTransSFDMP',
        atlasconfig: 'analytics-transATLAS/atlasconfig.json',
        analyticsTransATLAS: 'analytics-transATLAS/analyticsTransATLAS'
    }
});

require(['jquery', 'shop', 'mediator-js', 'analyticstracker', 'analyticsTransQA', 'analyticsTransDTM'], function($) {
	if (module = $('script[src$="require.js"]').data('module')) {
		require([module]);
	}
});