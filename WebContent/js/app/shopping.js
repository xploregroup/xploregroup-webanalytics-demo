
define (["jquery", "shop", "analyticstracker"], function($, Shop, analyticstracker) {
  require(['app/tracking', 'app/checkout']);
  var theShop = Shop();
  var shoptracker = analyticstracker();

  function removeItemFromCart(e) {
    shoptracker.trackInteraction(e);
    theShop.removeFromCart(JSON.parse(this.getAttribute("data-tracking-commerce")));
    var itemsInCart = theShop.renderCart($('#cart')[0]);
    if (itemsInCart == 0) {
      $('#cartIsEmpty').prop('class', 'collapse in');
      $('#checkout').prop('disabled', true);
    }
  }

  $(document).ready(function(){
    var mycart = theShop.getCart();

    if (document.title == "Analytics demo Shop - Cart") {
        if (mycart.length == 0) {
          $('#cartIsEmpty').prop('class', 'collapse in');
          $('#checkout').prop('disabled', true);
        }
        theShop.bindRemoveHandler(removeItemFromCart);
        theShop.renderCart($('#cart')[0]);
    } else if (document.title == "Analytics demo Shop - Orders") {
        var myorders = theShop.getOrders();
        if (myorders.length == 0) {
          $('#ordersIsEmpty').prop('class', 'collapse in');
          $('#clearorders').prop('disabled', true);
        }
        theShop.renderOrders($('#orders')[0]);
    }
  });

  $('[data-tracking-event$=-addtocart]').click(function(e) {
    theShop.addToCart(JSON.parse(this.getAttribute("data-tracking-commerce")));
    shoptracker.trackInteraction(e);
  });

  $('#clearorders').click(function(e) {
    theShop.removeOrders();
    theShop.renderOrders($('#orders')[0]);
  });
});
