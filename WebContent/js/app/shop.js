(function(root, factory) {
  'use strict';

  if (typeof define === 'function' && define.amd) {
    // AMD. Register as an anonymous module.
    define([], factory);
  } else if (typeof exports === 'object') {
    // Node. Does not work with strict CommonJS, but
    // only CommonJS-like enviroments that support module.exports,
    // like Node.
    module.exports = factory();
  } else {
    // Browser globals
    root.Shop = factory();
  }
}(this, function() {
  'use strict';

  var _instance;

  var Shop = function Shop() {
    if (!(this instanceof Shop)) {
      return new Shop();
    }

    this._cart = [];
    this._orders = [];
    this._removeHandler = null;

    // init the orderList
    this.getOrders();
  };

  Shop.prototype.getInstance = function() {
    if (!_instance) {
        _instance = Shop();
    }
    return _instance;
  }

  Shop.prototype.bindRemoveHandler = function (callback) {
    this._removeHandler = callback;
  }

  Shop.prototype.addToCart = function(item) {
    var id = item.id;
    var name = item.name;
    var quantity = Number(item.quantity);
    var price = Number(item.price);
    var totalPrice = Number(item.quantity) * Number(item.price);
    var brand = item.brand;
    var category = item.category;
    var itemFound = false;

    for (var i=0, len = this._cart.length; i < len; i++) {
      if (this._cart[i].id == id) {
        this._cart[i].quantity += quantity;
        this._cart[i].total = this._cart[i].quantity * this._cart[i].price;
        itemFound = true;
      }
    }
    if (! itemFound) {
      this._cart.push({"id" : id, "name" : name, "brand" : brand, "category" : category, "quantity" : quantity, "price" : price, "total" : totalPrice});
    }

    try {
      sessionStorage.setItem('shoppingCart', JSON.stringify(this._cart));
    } catch (e) {
      console.log("Demo shop error: " + e.message);
    }
  }

  Shop.prototype.removeFromCart = function(item) {
    var id = item.id;

    if (this._cart.length < 1) return;

    try {
      if (this._cart.length == 1) {
        this._cart = [];
        sessionStorage.removeItem('shoppingCart');
        return;
      }

      for (var i=0, len = this._cart.length; i < len; i++) {
        if (this._cart[i].id == id) {
          this._cart.splice(i, 1);
          sessionStorage.setItem('shoppingCart', JSON.stringify(this._cart));
          return;
        }
      }
    } catch (e) {
      console.log("Demo shop error: " + e.message);
    }
  }

  Shop.prototype.getCart = function() {
    var cartStorage = sessionStorage.getItem('shoppingCart');
    if (cartStorage != null) {
      this._cart = JSON.parse(cartStorage);
    }
    return this._cart;
  }

  Shop.prototype.totalCart = function() {
    var total = 0.0;

    for (var i=0, len = this._cart.length; i < len; i++) {
      total += this._cart[i].total;
    }

    return total;
  }

  Shop.prototype.getCartTotal = function(aCart) {
    var total = 0.0;

    for (var i=0, len = aCart.length; i < len; i++) {
      total += aCart[i].total;
    }

    return total;
  }

  Shop.prototype.emptyCart = function() {
    this._cart = [];
    try {
      sessionStorage.removeItem('shoppingCart');
    } catch (e) {
      console.log("Demo shop error: " + e.message);
    }
  }

  Shop.prototype.renderCart = function (el) {
    var childTable = document.getElementById("carttable");
    if (childTable !== null) { el.removeChild(childTable); }
    var cartTable = document.createElement('table');
    cartTable.setAttribute("id", "carttable");
    cartTable.setAttribute("width", "100%");
    cartTable.setAttribute("class", "table");
    var cartTHead = document.createElement("thead");
    var cartHeader = document.createElement("tr");
    cartHeader.innerHTML = '<th width="15%">ID</th><th width="35%">Name</th><th width="15%" class="text-right">Quantity</th><th width="15%" class="text-right">ItemPrice</th><th width="15%" class="text-right">Total</th><th width="5%"></th>';
    cartTHead.appendChild(cartHeader);
    cartTable.appendChild(cartTHead);

    var cartTBody = document.createElement("tbody");
    for (var i=0, len = this._cart.length; i < len; i++) {
        var productLine = document.createElement("tr");
        var item = this._cart[i];
        item.list = "cart";
        item.position = i+1;
        productLine.setAttribute("data-tracking-event", "product-impression");
        productLine.setAttribute("data-tracking-commerce", JSON.stringify(item));
        productLine.innerHTML = '<td>' + item.id + '</td><td>' + item.name + '</td><td class="text-right">' + item.quantity + '</td><td class="text-right">' + item.price + '</td><td class="text-right">' + item.total + '</td>';
        productLine.innerHTML += '<td class="text-right"><button id="removecart" class="btn btn-xs btn-danger" data-tracking-event="product-removefromcart" data-tracking-commerce=' + JSON.stringify(item) + '>x</button></td>';
        var removebutton = productLine.getElementsByTagName("button")[0];
        removebutton.onclick = this._removeHandler;
        cartTBody.appendChild(productLine);
    }
    var totalLine = document.createElement("tr");
    totalLine.setAttribute("class", "success");
    totalLine.innerHTML = '<td>TOTAL</td><td></td><td></td><td></td><td class="text-right">' + this.totalCart() + '</td><td></td>';
    cartTBody.appendChild(totalLine);
    cartTable.appendChild(cartTBody);
    el.appendChild(cartTable);

    return this._cart.length;
  }

  Shop.prototype.storeOrder = function(userId, orderId) {
    this._orders.push({"userId" : userId, "orderId" : orderId, "cart" : this._cart});
    try {
      sessionStorage.setItem('orders', JSON.stringify(this._orders));
    } catch (e) {
      console.log("Demo shop error: " + e.message);
    }
  }

  Shop.prototype.getOrders = function() {
    try {
      var orderStorage = sessionStorage.getItem('orders');
      if (orderStorage != null) {
        this._orders = JSON.parse(orderStorage);
      }
    } catch (e) {
      console.log("Demo shop error: " + e.message);
    }
    return this._orders;
  }

  Shop.prototype.removeOrders = function() {
    this._orders = [];
    try {
      sessionStorage.removeItem('orders');
    } catch (e) {
      console.log("Demo shop error: " + e.message);
    }
  }

  Shop.prototype.isCustomer = function(userId) {
    if (this._orders.length > 0) {
      for (var i=0, len = this._orders.length; i<len; i++) {
        if (this._orders[i].userId == userId) return true;
      }
    }

    return false;
  }

  Shop.prototype.renderOrders = function (el) {
    var childTable = document.getElementById("orderstable");
    if (childTable !== null) { el.removeChild(childTable); }
    var orderTable = document.createElement('table');
    orderTable.setAttribute("id", "orderstable");
    orderTable.setAttribute("width", "100%");
    orderTable.setAttribute("class", "table");
    var orderTHead = document.createElement("thead");
    var orderHeader = document.createElement("tr");
    orderHeader.innerHTML = '<th width="30%">userId</th><th width="35%">orderId</th><th width="15%" class="text-right">Quantity</th><th width="15%" class="text-right">Total</th><th width="5%"></th>';
    orderTHead.appendChild(orderHeader);
    orderTable.appendChild(orderTHead);

    var orderTBody = document.createElement("tbody");
    for (var i=0, len = this._orders.length; i < len; i++) {
        var orderLine = document.createElement("tr");
        var item = this._orders[i];
        item.list = "orders";
        item.position = i+1;
        orderLine.innerHTML = '<td>' + item.userId + '</td><td>' + item.orderId + '</td><td class="text-right">' + item.cart.length + '</td><td class="text-right">' + this.getCartTotal(item.cart) + '</td>';
        orderTBody.appendChild(orderLine);
    }
    orderTable.appendChild(orderTBody);
    el.appendChild(orderTable);

    return this._orders.length;
  }

  return Shop.prototype.getInstance;
}));
