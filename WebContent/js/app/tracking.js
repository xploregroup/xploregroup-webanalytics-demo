/*
 * Digital Data Layer
 *
 * Tracking implementation
 *
 * Stefan Maris
 * 2016-09-05
 *
 */
define (["jquery", "shop", "analyticstracker"], function($, shop, analyticstracker) {
	require(['app/tagPlan']);
	var atrack = analyticstracker();

	//atrack._htmlSelector.disableVisibilityCheck = true;

	trackOtherImpressions = function () {
		atrack.trackImpression("promotion-impression", {"doCollect" : false});

		if (document.title.match(/Analytics demo Shop/g)){
			// product lists
			atrack.trackImpression("product-impression", {"doCollect" : true});
			// product detail page
			atrack.trackImpression("product-detail-impression");
		} else {
			// track product impressions in Bootstrap Carousel
			atrack.trackElement($('#myCarousel li[data-tracking-event="product-impression"].active').not('[data-tracking-done="true"]'));
		}

		// check every second if new elements become visible; replaces an onscroll handler
		setTimeout(trackOtherImpressions, 1000);
	}

	$(document).ready(function(){
		// switch topheader based on customer or not
		var theShop = shop();
		if (theShop.isCustomer("demoUser")) {
			$('#visitorpromo').css('display','none');
			$('#customerpromo').css('display','block');
		}

		/*
		* Test setting. Rules only fire when the Consent s_cc cookie is set to true
		*/
		// analyticsTransQA().restrictedByConsentCookie("s_cc", "true");

		// early track test
		/*
		var trackingEvent = {"event": "button-click" , "info" : {"name" : "to-early"}};
		atrack.trackEvent(trackingEvent);
		*/

		// SPA check
		if (document.location.pathname == "/spa.html") {
			if (document.location.hash == "#fragment-home") {
				var pageInfoAttrElem = $('[data-tracking-event=navigation-click][id=ui-id-1]');
				atrack.trackElement(pageInfoAttrElem, {"changeEvent" : "page-impression", "newPage" : true});
			} else if (document.location.hash == "#fragment-campaign") {
				var pageInfoAttrElem = $('[data-tracking-event=navigation-click][id=ui-id-2]');
				atrack.trackElement(pageInfoAttrElem, {"changeEvent" : "page-impression", "newPage" : true});
			} else {
				atrack.trackImpression("page-impression");
			}
		} else {
			atrack.trackImpression("page-impression");
		}

		if (document.location.pathname == "/iframed.html") {
			var allowedIframes = [];
			allowedIframes.push("http://iframe.mytest2.local:3000");
			atrack.trackIFrames(allowedIframes);
		}
		trackOtherImpressions();
	});

	// interaction handlers
	$('[data-tracking-event$=-click]').not('[data-tracking-event=navigation-click]').click(atrack.trackInteraction);
	// SPA navigation
	$('[data-tracking-event=navigation-click]').click(function(e) {
		atrack.trackInteraction(e, {"changeEvent" : "page-impression", "newPage" : true});
	});
	$('[data-tracking-event=form-focus]').focus(atrack.trackInteraction);
	$('[data-tracking-event=form-focus]').change(function(e) {atrack.trackInteraction(e, {"changeEvent" : "form-fieldcomplete", "collectFormData" : ["username", "email"]});});
	$('[data-tracking-event=form-submit]').submit(function(e) {atrack.trackInteraction(e, {"collectFormData" : ["all"]}); e.preventDefault();});
});
