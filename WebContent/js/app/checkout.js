define (["jquery", "shop", "analyticstracker"], function($, Shop, analyticstracker) {
  var theShop = Shop();
  var shoptracker = analyticstracker();

  $(document).ready(function () {
      $('.checkout-form fieldset:first-child').fadeIn('slow');

      $('.checkout-form input[type="text"]').on('focus', function () {
          $(this).removeClass('input-error');
      });

      $('#checkout').on('click', function (e) {
          var trackingEvent = {"event": "checkout-step" , "info" : {"step" : 1}, "commerce" : theShop.getCart()};
          shoptracker.trackEvent(trackingEvent);
      });

      // next step
      $('.checkout-form .btn-next').on('click', function (e) {
          var parent_fieldset = $(this).parents('fieldset');
          var next_step = true;

          if (next_step) {
              parent_fieldset.fadeOut(400, function () {
                  $(this).next().fadeIn();
              });

              var currentStep = parent_fieldset[0].getAttribute("id");

              var theStep = 1;
              switch (currentStep) {
                case "step1" : theStep = 2; break;
                case "step2" : theStep = 3; break;
                case "step3" : theStep = 4; break;
                case "step4" : theStep = 5; break;
              }
              var trackingEvent = {"event": "checkout-step" , "info" : {"step" : theStep}, "commerce" : theShop.getCart()};
              shoptracker.trackEvent(trackingEvent);
          }

      });

      // order button
      $('.checkout-form .btn-success').on('click', function (e) {
          var d = new Date();
          var n = d.getTime();

          var orderID = "ORD" + n;

          $('.checkout-form #orderID')[0].value = orderID;

          var total = theShop.totalCart();
          var tax = total * 0.19;
          var shipping = total * 0.05;

          var trackingEvent = {"event": "checkout-purchase" , "info" : {'id': orderID,
          'affiliation': 'Online Store', 'revenue': total.toFixed(2), 'tax': tax.toFixed(2), 'shipping': shipping.toFixed(2)}, "commerce" : theShop.getCart()};
          shoptracker.trackEvent(trackingEvent);
          theShop.storeOrder("demoUser", orderID);

          theShop.emptyCart();
          theShop.renderCart($('#cart')[0]);

          var parent_fieldset = $(this).parents('fieldset');
          var next_step = true;

          if (next_step) {
              parent_fieldset.fadeOut(400, function () {
                  $(this).next().fadeIn();
              });
          }

      });

      $('.checkout-form #backtoshop').on('click', function (e) {
        document.location.href = "/shop.html";
      });

      // previous step
      $('.checkout-form .btn-previous').on('click', function () {
          $(this).parents('fieldset').fadeOut(400, function () {
              $(this).prev().fadeIn();
          });
      });
  });
});
