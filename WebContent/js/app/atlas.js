(function(){

  function renderTable (el, dataset) {
    var eventTable = document.createElement('table');
    eventTable.setAttribute("id", "eventTable");
    eventTable.setAttribute("width", "100%");
    eventTable.setAttribute("class", "table");
    var eventTHead = document.createElement("thead");
    var eventHeader = document.createElement("tr");
    eventHeader.innerHTML = '<th width="60%">Name</th><th width="40%">Count</th>';
    eventTHead.appendChild(eventHeader);
    eventTable.appendChild(eventTHead);

    var eventTBody = document.createElement("tbody");
    var totcount = 0;
    for (var i=0, len = dataset.length; i < len; i++) {
        var eventLine = document.createElement("tr");
        var item = dataset[i];
        eventLine.innerHTML = '<td>' + item._id + '</td><td>' + item.total + '</td>';
        eventTBody.appendChild(eventLine);
        totcount += item.total;
    }
    var totalLine = document.createElement("tr");
    totalLine.setAttribute("class", "success");
    totalLine.innerHTML = '<td>TOTAL</td><td>' + totcount + '</td>';
    eventTBody.appendChild(totalLine);
    eventTable.appendChild(eventTBody);
    el.appendChild(eventTable);
  }

  $('document').ready(function(){
    var appIdStitch = "myddlmod-vvxlr";
    var keyMongoDBAtlasStitch = {"name" : "transATLASkeyAPI", "key" : "ndq1RCqZlcsFrX0smayLCyNTH1fhGCFmzqtKjxTNlj7ag8uXnA1BKHZRpGI3lC1i"};

    var sClient = new stitch.StitchClient(appIdStitch);

    sClient.authenticate("apiKey", keyMongoDBAtlasStitch.key).then(function () {
      var mongodb = sClient.service("mongodb", "mongodb-atlas").db("tracking");
      var ddlData = mongodb.collection("webevents");

      // MongoDB Aggregation pipeline
      var mongoPipe = [
        { $match : {} },
        { $group : {_id : "$data.event", total: { $sum: 1} } }
      ];

      ddlData.aggregate(mongoPipe).then(function (docs) {
        renderTable($('#eventTable')[0], docs);
      });
    });
  });
})();
