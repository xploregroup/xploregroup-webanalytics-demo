window.availableEvents = [
      {"event" : "page-impression", "rule" : "page-impression", "infoFields" : ["author*"], "commerceFields" : []},
      {"event" : "navigation-click", "rule" : "navigation-click", "infoFields" : ["url*","title*"], "commerceFields" : []},
      {"event" : "promotion-impression", "rule" : "promotion-impression", "infoFields" : ["campaign*", "name*"], "commerceFields" : []},
      {"event" : "promotion-click", "rule" : "promotion-click", "infoFields" : ["campaign*", "name*"], "commerceFields" : []},
      {"event" : "product-addtocart", "rule" : "product-addtocart", "infoFields" : [], "commerceFields" : ["id*", "name*", "price*", "quantity*"]},
      {"event" : "product-removefromcart", "rule" : "product-removefromcart", "infoFields" : [], "commerceFields" : ["id*", "name*", "price*", "quantity*"]},
      {"event" : "campaign-link-click", "rule" : "campaign-link-click", "infoFields" : ["type", "target*"], "commerceFields" : []},
      {"event" : "product-impression", "rule" : "product-impression", "infoFields" : ["name"], "commerceFields" : ["id*", "name*", "price*", "list*", "position*"]},
      {"event" : "product-detail-impression", "rule" : "product-detail-impression", "infoFields" : [], "commerceFields" : ["id*", "name*", "price*", "list", "position"]},
      {"event" : "product-click", "rule" : "product-click", "infoFields" : ["name"], "commerceFields" : ["id*", "name*", "price*", "list*", "position*"]},
      {"event" : "form-focus", "rule" : "form-focus", "infoFields" : ["formId*", "field*"], "commerceFields" : []},
      {"event" : "form-submit", "rule" : "form-submit", "infoFields" : ["formId*"], "commerceFields" : []}
    ];
