
// we need the express framework
var express = require('express')
var vhost = require('vhost')
var cookieParser = require('cookie-parser')
const parseDomain = require("parse-domain");
const uuidv1 = require('uuid/v1');
var Fingerprint = require('express-fingerprint')
var medea = require('medea');

// create app to serve static files on subdomain www.
var staticapp = express()

staticapp.use(cookieParser());
staticapp.use(express.static(__dirname+'/../WebContent'))

// Visitor ID tracking server on subdomain vid.
/*
 * The server uses serverside fingerprinting on the incoming request (useragent, acceptheaders, geoip)
 * The server uses a simple key-value DB to store fingerprint/uuid combi's
 * The server can operate over different domains, allowing cross-domain tracking (if visit from same browser)
 * The server sets a Http-Cookie with the visitor ID (uuid) in t_vid
 */
var trackingapp = express();

var db = medea();

trackingapp.use(cookieParser());
trackingapp.use(Fingerprint());
trackingapp.get('/', function(req, res) {
    if ((Object.keys(req.cookies).length > 0) && req.cookies.hasOwnProperty('t_vid')) {
        console.log("tracker vid already set to: " + req.cookies.t_vid)
        res.send('');
    } else {
        // check browser fingerprint in our local DB
        db.open(function(err) {
            console.log("try to retrieve fingerprint " + req.fingerprint.hash + " from DB")
            db.get(req.fingerprint.hash, function (err, val) {
                var uid = "";
                if (err !== null) {
                    console.log(err);
                    // we don't have the fingerprint yet... write it
                    uid = uuidv1();
                    db.put(req.fingerprint.hash, uid, function (err) {
                        console.log("write fingerprint: " + req.fingerprint.hash);
                        db.close(function (err) {});
                    });
                } else {
                    // we found an ID for this fingerprint, use it
                    console.log("found a val: " + val.toString());
                    uid = val.toString();
                    db.close(function (err) {});
                }
                var dmn = parseDomain(req.hostname, {customTlds: /localhost|\.local/});
                res.cookie("t_vid", uid, { domain: dmn.domain+"."+dmn.tld, path: '/', expires: new Date(Date.now() + 2*365*24*60*60*1000)});
                res.send('');
            });
        });
    }
  });

// create app to serve static files on subdomain www.
var iframeapp = express()

iframeapp.use(cookieParser());
iframeapp.use(express.static(__dirname+'/../IFrame'))

// around max host connections limit on browsers
// create main app
var app = express()

app.use(vhost('www.mytest.local', staticapp))
app.use(vhost('iframe.mytest2.local', iframeapp))
app.use(vhost('vid.mytest.local', trackingapp))
app.use(vhost('vid.mytest2.local', trackingapp))

// listen on port 3000
app.listen(3000);