require.config({
    baseUrl: 'js/lib',
    paths: {
        app: '../app',
        'mediator-js': 'mediator-js/mediator.min',
        analyticstracker: 'analyticstracker/analyticstracker',
        analyticsTransQA: 'analytics-transQA/analyticsTransQA',
        analyticsTransDTM: 'analytics-transDTM/analyticsTransDTM',
        analyticsTransIFrame: 'analytics-transIFrame/analyticsTransIFrame'
    }
});

require(['jquery', 'mediator-js', 'analyticstracker', 'analyticsTransQA', 'analyticsTransIFrame'], function($) {
	if (module = $('script[src$="require.js"]').data('module')) {
		require([module]);
	}
});