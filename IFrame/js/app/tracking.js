/*
 * Digital Data Layer
 *
 * Tracking implementation
 *
 * Stefan Maris
 * 2016-09-05
 *
 */
define (["jquery", "analyticstracker", "analyticsTransIFrame"], function($, analyticstracker, analyticsTransIFrame) {
	require(['app/tagPlan']);
	var atrack = analyticstracker();

	var getParamsMap = function () {
		var params = location.search.replace('?', '').split("&");
		var paramsMap = {};
		params.forEach(function (p) {
			var v = p.split("=");
			paramsMap[v[0]]=decodeURIComponent(v[1]);
		});
		return paramsMap;
	};

	// add iFrame tracking if the page is iFramed
	// no postMessages are sent if no URL's are added...
	// the parent URL is passed via the iframed param
	if(location.search.indexOf("iframed") > -1) {
		var aIframeTrans = analyticsTransIFrame();
		// aIframeTrans.addParentUrl("http://www.mytest.local:3000");
		// aIframeTrans.addParentUrl("http://www.mytest2.local:3000");
		aIframeTrans.addParentUrl(getParamsMap()["iframed"]);
	} else {
		// direct page load, track as usual...
		require(['analyticsTransDTM']);
	}

	$(document).ready(function(){
		atrack.trackImpression("page-impression");
	});

	$('[data-tracking-event=form-focus]').focus(atrack.trackInteraction);
	$('[data-tracking-event=form-focus]').change(function(e) {atrack.trackInteraction(e, {"changeEvent" : "form-fieldcomplete", "collectFormData" : ["username", "email"]});});
	$('[data-tracking-event=form-submit]').submit(function(e) {atrack.trackInteraction(e, {"collectFormData" : ["all"]}); e.preventDefault();});
});
