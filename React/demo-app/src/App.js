import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';
import analyticstracker from 'analyticstracker';
import Banner from './Banner.js';
import Registerform from './Registerform.js';

class App extends Component {
  constructor(props) {
    super(props);
    this.tracker = analyticstracker();
    this.timer = 0;
    this.state = {
            defaultbanner: true,
        };

    this.startTimer = this.startTimer.bind(this);
    this.checkImpressions = this.checkImpressions.bind(this);
    this.handleChangeBanner = this.handleChangeBanner.bind(this);
  }

  componentDidMount() {
    this.tracker.trackImpression("page-impression");
    this.startTimer();
  }

  startTimer() {
    if (this.timer == 0) {
      this.timer = setInterval(this.checkImpressions, 1000);
    }
  }

  checkImpressions() {
    this.tracker.trackImpression("promotion-impression", {doCollect : true});
  }

  handleChangeBanner (e) {
    this.setState({defaultbanner : !this.state.defaultbanner});
  }

  render() {
    // const children = this.props.children;
    return (
      <div className="App">
        <header className="App-header">
          <img src={logo} className="App-logo" alt="logo" />
        </header>
        <p></p>
        <div className="Content">
          <div className="TopBanner" id="Topbanner">
              {this.state.defaultbanner && <Banner key="topbanner" banner-image='./images/promo2.jpg' analytics-info='{"name" : "testBanner", "campaign": "ABC", "position" : "top"}'/>}
              {! this.state.defaultbanner && <Banner key="othertopbanner" banner-image='./images/promo3.jpg' analytics-info='{"name" : "saleBanner", "campaign": "XYZ", "position" : "top"}'/>}
          </div>
          <div className="Article" id="someothercontent">
            <p> Some example single page React app, that shows how the analytics tracking events can be exposed throug the React App framework.
            It also allows to change the header banner <br/><br/><button className="btn btn-success" onClick={this.handleChangeBanner}>Change Banner</button>
            <br/><br/>Register below to get a free gift!</p>
          </div>
          <div className="Proposition">
            <Banner banner-image='./images/promo1.jpg' analytics-info='{"name" : "bottomBanner", "campaign": "DEF", "position" : "bottom"}'/>
            <Registerform analytics-info='{"name" : "registrationform", "formId" : "formREG1"}' />
          </div>
        </div>
      </div>
    );
  }
}

export default App;
