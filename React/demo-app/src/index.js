import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import registerServiceWorker from './registerServiceWorker';
/*eslint-disable no-unused-vars*/
import analyticsTransQA from 'analytics-transQA';
import analyticsTransGTM from 'analytics-transGTM';
/*eslint-enable no-unused-vars*/

ReactDOM.render(<App />, document.getElementById('root'));
registerServiceWorker();
