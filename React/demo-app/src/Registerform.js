import React, { Component } from 'react';
import './Registerform.css';
import analyticstracker from 'analyticstracker';

class Registerform extends Component {
  props: {
    'analytics-info': string
  }

  handleFocus(e) {
    let aTrack = analyticstracker();
    aTrack.trackInteraction(e);
  }

  handleChange(e) {
    let aTrack = analyticstracker();
    aTrack.trackInteraction(e, {"changeEvent" : "form-fieldcomplete", "collectFormData" : ["username", "email"]})
  }

  handleSubmit(e) {
    let aTrack = analyticstracker();
    aTrack.trackInteraction(e, {"collectFormData" : ["username", "email"]});
    e.preventDefault();
  }

  render() {
    let usernameInfo = JSON.parse(this.props['analytics-info']), emailInfo = JSON.parse(this.props['analytics-info']);
    usernameInfo.field = "username";
    emailInfo.field = "email";
    return (
      <div className="Form">
        <form id="nameform" onSubmit={this.handleSubmit} data-tracking-event="form-submit" data-tracking-info={this.props['analytics-info']}>
  				 <div className="form-group">
  						<label className="form-label">Name: </label>
  			 			<input className="form-control" type="text" name="username" onFocus={this.handleFocus} onBlur={this.handleChange} data-tracking-event="form-focus" data-tracking-info={JSON.stringify(usernameInfo)} />
  						<br/><label className="form-label">Email: </label>
  						<input className="form-control" type="text" name="email" onFocus={this.handleFocus} onBlur={this.handleChange} data-tracking-event="form-focus" data-tracking-info={JSON.stringify(emailInfo)} />
  					</div>
  			<button type="submit" className="btn btn-success">Subscribe</button>
  		 </form>
      </div>
    )
  }
}

export default Registerform; // Don’t forget to use export default!
