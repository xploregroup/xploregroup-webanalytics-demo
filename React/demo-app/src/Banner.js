import React, { Component } from 'react';
import './Banner.css';
import analyticstracker from 'analyticstracker';

class Banner extends Component {
  props: {
    'banner-image': string,
    'analytics-info': string
  }

  componentDidMount() {
  }

  handleClick = (e) => {
    let aTrack = analyticstracker();
    aTrack.trackInteraction(e);
    e.preventDefault();
  }

  render() {
    return (
      <div className="Banner" data-tracking-event="promotion-impression" data-tracking-info={this.props['analytics-info']}>
        <a href="" data-tracking-event="promotion-click" data-tracking-info={this.props['analytics-info']} onClick={this.handleClick}>
          <img src={this.props['banner-image']} alt="banner"></img>
        </a>
      </div>
    )
  }
}

export default Banner; // Don’t forget to use export default!
